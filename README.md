### Initial preparation

The customized config files (such as config.h) shall be stored in `/root/config_files/[pkg_name]`.
 ```
  $ su -
  $ cd void-packages
```
### Clean up
```
  $ ./xbps-src clean
```  
### Fetch and extract source files, customize config file

Config file can be edited now if changes are needed. Extract the packag efirst.
```
  $ ./xbps-src extract *package*
```

If the new (updated) config file from repository is different than custom one, copy the default config to a new config.h, edit and save. Update the custom config and git commit.
```
  $ cp masterdir/builddir/[pkg_dir]/config.def.h masterdir/builddir/[pkg_dir]/config.h
  $ vim masterdir/builddir/[pkg_dir]/config.h
  $ cp masterdir/builddir/[pkg_dir]/config.h ~/config_files/[pkg_name]/
```

Otherwise just copy the custom config to the build directory.
```
  $ cp ~/config_files/[pkg_name/config_file] masterdir/builddir/[pkg_name]
```
### Compile, install to a local repository and test

Compile and install. The binary file and possibly other files will be under masterdir/destdir/[pkg_dir]
```
  $ ./xbps-src install [pkg]
```

Program can be tested by running the binary file in destdir. If all seems to be ok, copy the destdir files to their final locations (such as /usr/bin).
